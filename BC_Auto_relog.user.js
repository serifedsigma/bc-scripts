// ==UserScript==
// @name         BC Auto relog
// @namespace    BCPatches
// @version      0.1
// @description  Stores the password you enter on login and automatically tries to re-log if you get disconnected
// @author       serif#8333 (you can find me on BC / BCX discords)
// @include      /^https:\/\/(www\.)?bondageprojects\.elementfx\.com\/R\d+\/(BondageClub|\d+)(\/((index|\d+)\.html)?)?$/
// @include      /^https:\/\/(www\.)?bondage-europe\.com\/R\d+\/(BondageClub|\d+)(\/((index|\d+)\.html)?)?$/
// @runat        document-end
// @grant        none
// ==/UserScript==

setTimeout(() => {
    const script = document.createElement("script");
    script.text = `
let SavedPassword;


function TryRelog() {
	if(ServerIsConnected && !LoginSubmitted) {
		const AccountName = Player.AccountName;
		const Password = SavedPassword;

		LoginSetSubmitted();
		ServerSend("AccountLogin", { AccountName, Password });
	}
}

console.log("BC Auto relog patching login functions");

const originalLoginDoLogin = window.LoginDoLogin;
window.LoginDoLogin = function LoginDoLogin() {
	if (!LoginSubmitted && ServerIsConnected) {
		SavedPassword = ElementValue("InputPassword");
	}

	originalLoginDoLogin.apply(this);
}

const originalRelogLoad = window.RelogLoad;
window.RelogLoad = function RelogLoad() {
	originalRelogLoad.apply(this);

	// try to relog a few times, then give up
	let relogTries = 5;
	let relogInterval = setInterval(() => {
		if(CurrentScreen == "Relog" && relogTries > 0) {
			TryRelog();
			relogTries--;
		} else {
			clearInterval(relogInterval);
		}
	}, 1000);
}

console.log("Patched");`;

    document.head.appendChild(script);
},
1000);
